/********************************* Get the input ********************************************************/

let input = document.getElementById("input").innerHTML.split("\n");
console.log('input', input);

/******************************** Classes ***************************************************************/

class Party {
	constructor(members) {
		this.members = members;
	}
}

class Elf {
	constructor(rations) {
	
		this.rations = rations;
		this.totalCaloriesCarried = 0;
	
		for (let i = 0; i < rations.length; i++){
			this.totalCaloriesCarried += this.rations[i].calories;
		}
	}
}

class Snack {
	constructor(calories) {
		this.calories = calories;
		
	}
}

/*************************** Procedurals *************************************************************/

function buildParty() {
	let elves = new Party([]);
	let rations = [];
	
	for (let i = 0; i < input.length; i++) {
		if (input[i] !== "" && input[i] !== "\t") {
			rations.push(new Snack(parseInt(input[i])));
		} else {
			elves.members.push(new Elf(rations));
			rations = [];
		}
	}
	return elves;
}

let elves = buildParty();

function sortTopThree(newValue, topThree) {
	if (typeof topThree === "undefined"){
		topThree = {first: 0, second: 0, third: 0};
	}
	
	if (newValue > topThree.first) {
		topThree.third = topThree.second;
		topThree.second = topThree.first;
		topThree.first = newValue;
	} else if (newValue > topThree.second ) {
		topThree.third = topThree.second;
		topThree.second = newValue;
	} else if (newValue > topThree.third) {
		topThree.third = newValue;
	}
	
	return topThree;	
}

/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

// Get Answer
( function () {
	let topThree;
	
	for (let i = 0; i < elves.members.length; i++) {
		let calories = elves.members[i].totalCaloriesCarried;
		topThree = sortTopThree(calories, topThree);
	}
	
	let answer = topThree.first + topThree.second + topThree.third;
	
	addAnswerToPage(answer);
})();