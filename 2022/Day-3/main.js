/********************************* Get the input ********************************************************/
let separator = '----------------------------------------------------------';
let input = document.getElementById("input").innerHTML.split("\n");
console.log('input', input);

/******************************** Classes ***************************************************************/

class Group {
	constructor (rucksacks) {
		this.rucksacks = rucksacks;
		
		console.log(separator);
		
		for (let i = 0; i < rucksacks[0].compartments.length; i++) { // look in each compartment
			for (let j = 0; j < rucksacks[0].compartments[i].items.length; j++) { //look at each item 
				for (let k = 0; k < rucksacks[1].compartments.length; k++) { // look in each compartment of the second rucksack
					for (let l = 0; l < rucksacks[1].compartments[k].items.length; l++) { // look at each item in the second rucksack
						if (rucksacks[0].compartments[i].items[j].priority === rucksacks[1].compartments[k].items[l].priority) {
							// we found a common item between the first two rucksacks
							for (let m = 0; m < rucksacks[2].compartments.length; m++) { // look in each compartment of the third rucksack
								for (let n = 0; n < rucksacks[2].compartments[m].items.length; n++) { // look at each item in the third rucksack
									if (rucksacks[0].compartments[i].items[j].priority === rucksacks[2].compartments[m].items[n].priority) {
										this.badge = rucksacks[2].compartments[m].items[n].priority;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

class Rucksack {
	constructor(compartments) {
		if (typeof compartments === 'undefined') {
			this.compartments = [];
		} else {
			this.compartments = compartments;
		}
		
		for (let i = 0; i < this.compartments[0].items.length; i++) {
			for (let j = 0; j < this.compartments[1].items.length; j++) {
				if (this.compartments[0].items[i].priority === this.compartments[1].items[j].priority) {
					this.duplicate = this.compartments[0].items[i].priority;
				}
			}
		}
	}
}

class Compartment {
	constructor () {
		this.items = [];
	}
}

class Item {
	constructor (type) {
		this.type = type;
		if (this.type.match( /[a-z]/ )) {
			this.priority = parseInt(this.type, 36) - 9;
		} else if (this.type.match( /[A-Z]/)) {
			this.priority = parseInt(this.type, 36) + 17;
		} else {
			this.priority = "unknown";
		}
	}
}

/*************************** Procedurals *************************************************************/

function putItemsInRucksack (items) {
	let numberOfItemsPerCompartment = items.length / 2;
	let compartment1 = new Compartment();
	let compartment2 = new Compartment();
	for (let i = 0; i < items.length; i++) {
		let item = new Item(items[i]);
		if (i < numberOfItemsPerCompartment) {
			compartment1.items.push(item);
		} else {
			compartment2.items.push(item);
		}
	}
	return  new Rucksack([compartment1, compartment2]);
}


function buildRucksacks() {
	let rucksacks = [];
	for (let i = 0; i < input.length; i++) {
		rucksacks.push(putItemsInRucksack(input[i]));
	}
	
	return rucksacks;
}

function formGroups(members) {
	let groups = [];
	for (let i = 0; i < members.length; i += 3) {
		let group = [];
		for (let j = 0; j <= 2; j++) {
			group.push(members[i + j]);
		}
		groups.push(new Group(group));
	}
	
	return groups;
}

let rucksacks = buildRucksacks();
let groups = formGroups(rucksacks);
console.log('groups', groups);	
// }

/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

// Get Answer
( function () {
	let answer = 0;
	
	for (let i = 0; i < groups.length; i++) {
		answer += groups[i].badge;
	}
	
	addAnswerToPage(answer);
})();