/********************************* Get the input ********************************************************/
let separator = '----------------------------------------------------------';
let input = document.getElementById("input").innerHTML.split("\n");
console.log('input', input);


/******************************** Classes ***************************************************************/

class Tree {
	constructor(height, position, visible) {
		this.height = parseInt(height);
		this.position = position;
		// this.position.column = this.position.x;
		// this.position.row = this.position.y;
		
		if (typeof visible === 'undefined') {
			this.visible = false;
		}
		this.viewingDistance = {
			north: 0,
			south: 0,
			east: 0,
			west: 0
		};
	}
}

class Line {
	constructor(id, orientation) {
		this.id = id;
		this.orientation = orientation;
	}
}

class Forest {
	constructor(rows, columns) {
		this.rows = rows;
		this.columns = columns;
	}
}

/*************************** Procedurals *************************************************************/
function getViewingDistances(forest) {
	for (let i = 0; i < forest.rows.length; i++) {
		for (let j = 0; j < forest.rows[i].length; j++) {
			let tree = forest.rows[i][j];
			
			//north
			if (tree.position.y !== 0) {
				
				for (let row = tree.position.y - 1; row >= 0 ; row--) {
					tree.viewingDistance.north++;
					if (forest.columns[tree.position.x][row].height >= tree.height) {
						row = -1;
					}
				}
			}
			
			//south
			if (tree.position.y !== forest.rows.length) {
				for (let row = tree.position.y + 1; row < forest.columns.length ; row++) {
					tree.viewingDistance.south++;
					if (forest.columns[tree.position.x][row].height >= tree.height) {
						row = forest.columns.length;
					}
				}
			}
			
			//east
			if (tree.position.x !== forest.columns.length) {
				for (let column = tree.position.x + 1; column < forest.rows[i].length ; column++) {
					tree.viewingDistance.east++;
					if (forest.rows[tree.position.y][column].height >= tree.height) {
						column = forest.rows.length;
					}
				}
			}
			
			//west
			if (tree.position.x !== 0) {
				for (let column = tree.position.x - 1; column >= 0 ; column--) {
					tree.viewingDistance.west++;
					if (forest.rows[tree.position.y][column].height >= tree.height) {
						column = -1;
					} 
				}
			}
			
			tree.viewingDistance.total = tree.viewingDistance.north * tree.viewingDistance.south * tree.viewingDistance.east * tree.viewingDistance.west;
		}
	}
}


function checkLine(line, xOrY) {
	// check forward
	for (let i = 0; i < line.length; i++) {
		
		if (!line[i].visible) {
			
			let inBetweenTreesAreAllShorter = true;
			
			for (let j = 0; j < line[i].position[xOrY] ; j++) {
				
				if (line[j].height >= line[i].height) {
					inBetweenTreesAreAllShorter = false;
					j = line[i].position[xOrY];
				}
			}
			
			if (inBetweenTreesAreAllShorter) {
				line[i].visible = true;
			} 
		}	
	}
	
	// check backward
	for (let i = 0; i < line.length; i++) {
		
		if (!line[i].visible) {
			
			let inBetweenTreesAreAllShorter = true;
			
			for (let j = line[i].position[xOrY] + 1; j < line.length ; j++) {
				
				if (line[j].height >= line[i].height) {
					inBetweenTreesAreAllShorter = false;
					j = line.length;
				}
			}
			
			if (inBetweenTreesAreAllShorter) {
				line[i].visible = true;
			} 
		}
	}
}

function markVisibleTrees(forest) {
	for (let i = 0; i < forest.rows.length; i++) {
		checkLine(forest.rows[i], 'x');
	}
	
	for (let i = 0; i < forest.columns.length; i++) {
		checkLine(forest.columns[i], 'y');
	}
}

function createForest() {
	let rows = [];
	let columns = [];
	for (let i = 0; i < input.length; i++) {
		let row = [];
		for (let j = 0; j < input[i].length; j++) {
			let newTree = new Tree(input[i].slice(j, j + 1), {x: j, y:i});
			if (typeof columns[j] === 'undefined') {
				columns.push([]);
			}
			
			if (newTree.position.x === 0 || newTree.position.y === 0 || newTree.position.x === input[i].length - 1 || newTree.position.y === input.length - 1) {
				newTree.visible = true;
			}
			
			columns[j].push(newTree);
			row.push(newTree);
			
		}
		rows.push(row);
	}
	
	let forest = new Forest(rows, columns);
	return forest;
}

let forest = createForest();


markVisibleTrees(forest);
getViewingDistances(forest);
console.log('forest', forest);


/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

function getAnswer() {
  let answer = 0;
  
  for (let i = 0; i < forest.rows.length; i++) {
  	for (let j = 0; j < forest.rows[i].length; j++) {
  		if (forest.rows[i][j].viewingDistance.total >= answer) {
  			answer = forest.rows[i][j].viewingDistance.total;
  		}
  	}
  }
  
  return answer;
}

addAnswerToPage(getAnswer());
