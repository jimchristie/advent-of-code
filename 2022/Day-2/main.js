/********************************* Get the input ********************************************************/

let input = document.getElementById("input").innerHTML.split("\n");
console.log('input', input);

// The first column is what your opponent is going to play: 
// A for Rock, 
// B for Paper, and 
// C for Scissors. 

// The second column,  win lose or draw: 
// X for lose, 
// Y for draw, and 
// Z for win. 

// The score for a single round is the score for the shape you selected (
// 1 for Rock, 
// 2 for Paper, and 
// 3 for Scissors) 

// plus the score for the outcome of the round (
// 0 if you lost, 
// 3 if the round was a draw, and 
// 6 if you won).

/******************************** Classes ***************************************************************/

class Round {
	constructor (opponentThrow, winLoseOrDraw) {
		this.winLoseOrDraw = winLoseOrDraw;
		this.opponentThrow = opponentThrow;
		this.myScore = 0;
		this.opponentScore = 0;
		
		switch (this.winLoseOrDraw) {
			case "win":
				this.myScore += 6;
				if (this.opponentThrow === "rock") {
					this.myThrow = "paper";
				} else if (this.opponentThrow === "paper") {
					this.myThrow = "scissors";
				} else if (this.opponentThrow === "scissors") {
					this.myThrow = "rock";
				}
				break;
				
			case "lose":
				this.opponentScore += 6;
				if (this.opponentThrow === "rock") {
					this.myThrow = "scissors";
				} else if (this.opponentThrow === "paper") {
					this.myThrow = "rock";
				} else if (this.opponentThrow === "scissors") {
					this.myThrow = "paper";
				}
				break;
				
			case "draw":
				this.myScore += 3;
				this.opponentScore += 3;
				this.myThrow = this.opponentThrow;
				break;
				
		}
		
		// throw scoring
		switch (this.myThrow) {
			case "rock":
				this.myScore += 1;
				break;
				case "paper":
				this.myScore += 2;
				break;
				case "scissors":
				this.myScore += 3;
				break;
				
		}
		
		switch (this.opponentThrow) {
			case "rock":
				this.opponentScore += 1;
				break;
				case "paper":
				this.opponentScore += 2;
				break;
				case "scissors":
				this.opponentScore += 3;
				break;
		}
	}

}

class Game {
	constructor (rounds) {
		this.rounds = rounds;
		this.myScore = 0;
		this.opponentScore = 0;
		for (let i = 0; i < this.rounds.length; i++) {
			this.myScore += this.rounds[i].myScore;
			this.opponentScore += this.rounds[i].opponentScore;
		}
		
		
	}
}

/*************************** Procedurals *************************************************************/


function convertInputs(input) {
	input = input.split(" ");
	let sanitizedThrows = [];
	for (let i = 0; i < input.length; i++ ) {
		switch (input[i]) {
			case "A":
				sanitizedThrows[i] = "rock";
				break;
			case "B":
				sanitizedThrows[i] = "paper";
				break;
			case "C":
				sanitizedThrows[i] = "scissors";
				break;
			case "X":
				sanitizedThrows[i] = "lose";
				break;
			case "Y":
				sanitizedThrows[i] = "draw";
				break;
			case "Z":
				sanitizedThrows[i] = "win";
				break;
		}
	}
	
	return sanitizedThrows;
}


function buildRounds() {
	let rounds = [];
	for (let i = 0; i < input.length; i++) {
		let sanitizedThrows = convertInputs(input[i]);
		
		let opponentThrow = sanitizedThrows[0];
		let myThrow = sanitizedThrows[1];
		
	rounds.push(new Round(opponentThrow, myThrow));
	}
	
	return rounds;
}

let rounds = buildRounds();
let game = new Game(rounds);
console.log('game', game);	
// }

/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

// Get Answer
( function () {
	let answer = game.myScore;
	addAnswerToPage(answer);
})();