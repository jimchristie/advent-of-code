/********************************* Get the input ********************************************************/
let separator = '----------------------------------------------------------';
let input = document.getElementById("input").innerHTML.split("\n");
console.log('input', input);


/******************************** Classes ***************************************************************/

class Directory {
	constructor (name, parent) {
		this.name = name;
		if (typeof parent !== "undefined") {
			this.parentDirectory = parent;
		}
		this.files = [];
		this.subdirectories = [];
	}
}

class File {
	constructor(name, size) {
		this.name = name;
		this.size = parseInt(size);
	}
}

class TerminalOutput {
	constructor(readout) {
		this.readout = readout;
		this.type = readout.slice(0, 1) === '$' ? 'command' : 'listing';
	}
}

/*************************** Procedurals *************************************************************/


function parseInput() {
	let parsedInput = [];
	for (let i = 0; i < input.length; i++) {
		parsedInput.push(new TerminalOutput(input[i]));
	}
	
	return parsedInput;
}


let terminalCommands = parseInput();
console.log('terminalCommands', terminalCommands);


function buildFileSystem (outputs) {
	let fileSystem = new Directory('/');
	let currentDirectory = fileSystem;
	
	for (let i = 0; i < outputs.length; i++) {
		if (outputs[i].type === 'command') {
			// executeCommand(outputs[i].readout);
			if (outputs[i].readout.slice(2, 4) === 'cd') {
				let targetDirectory = outputs[i].readout.slice(5);
				if (targetDirectory === "..") {
					currentDirectory = currentDirectory.parentDirectory;
				} else if (targetDirectory === '/') {
					currentDirectory = fileSystem;
				} else {
					for (let subDirNum = 0; subDirNum < currentDirectory.subdirectories.length; subDirNum++) {
						if (currentDirectory.subdirectories[subDirNum].name === targetDirectory) {
							currentDirectory = currentDirectory.subdirectories[subDirNum];
						}
					}
				}
			}
				
		} else {
			let j = i;
			let filesFound;
			do {
				filesFound =  outputs[j].type === 'readout';
				let listingExists = false;
				for (let k = 0; k < currentDirectory.files.length; k++) {
					let listedFileOrDirectory = outputs[i].readout.split(' ')[1];
					if (currentDirectory.files[k].name === listedFileOrDirectory) {
						listingExists = true;
					}
				}
				
				if (!listingExists) {
					if (outputs[i].readout.slice(0, 3) === 'dir') {
						currentDirectory.subdirectories.push(new Directory(outputs[i].readout.slice(4, outputs[i].readout.length), currentDirectory));
					} else {
						let fileInfo = outputs[i].readout.split(' ');
						currentDirectory.files.push(new File(fileInfo[1], fileInfo[0]));
					}
				}
				if (j < i) {
					j++;
				}
			}
			while (filesFound);
		}
	}
	
	return fileSystem;
}

let fileSystem = buildFileSystem(terminalCommands);


function getFileSizes(directory) {
	let directorySize = 0;
	if (directory.subdirectories.length > 0) {
		for (let i = 0; i < directory.subdirectories.length; i++) {
			getFileSizes(directory.subdirectories[i]);
			directorySize += directory.subdirectories[i].size;
		}
	}
		
	if (directory.files.length > 0) {
		for (let i = 0; i < directory.files.length; i++) {
			directorySize += directory.files[i].size;
		}
	}
	
	
	directory.size = directorySize;
}

let systemSize = getFileSizes(fileSystem);

let maxFileSize = 100000;

/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

function getAnswer(directory, currentAnswer) {
  let spaceNeeded = 30000000;
	
	if (directory.size < currentAnswer && directory.size + fileSystem.freeSpace >= spaceNeeded) {
		currentAnswer = directory.size;
	}
	
	for (let i = 0; i < directory.subdirectories.length; i++) {
		currentAnswer = getAnswer(directory.subdirectories[i], currentAnswer);
	}
	
	return currentAnswer;
}

fileSystem.maxSize = 70000000;
fileSystem.freeSpace = fileSystem.maxSize - fileSystem.size;

addAnswerToPage(getAnswer(fileSystem, fileSystem.size));
