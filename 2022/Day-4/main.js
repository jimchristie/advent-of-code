/********************************* Get the input ********************************************************/
let separator = '----------------------------------------------------------';
let input = document.getElementById("input").innerHTML.split("\n");
console.log('input', input);

/******************************** Classes ***************************************************************/

class Section {
	constructor(range) {
		this.range = range;
		range = range.split('-');
		this.bottom = parseInt(range[0]);
		this.top = parseInt(range[1]);
	}
}

class CleaningPair {
	constructor (sections) {
		this.sections = sections;
		this.overlaps = false;
		this.fullyOverlaps = false;
		
		if (
			(this.sections[0].bottom <= this.sections[1].bottom && this.sections[0].top >= this.sections[1].top) ||
			(this.sections[1].bottom <= this.sections[0].bottom && this.sections[1].top >= this.sections[0].top) ) {
				this.fullyOverlaps = true;
				this.overlaps = true;
		} else if (
			(this.sections[0].bottom <= this.sections[1].bottom && this.sections[0].top >= this.sections[1].bottom) ||
			(this.sections[0].top >= this.sections[1].top && this.sections[0].bottom <= this.sections[1].top) ) {
				this.overlaps = true;
		} 
	}
}
/*************************** Procedurals *************************************************************/



function formPairs () {
	for (let i = 0; i < input.length; i++) {
		let splitInput = input[i].split(',');
		let sections = [];
		sections.push(new Section(splitInput[0]));
		sections.push(new Section(splitInput[1]));
		
		cleaningPairs.push(new CleaningPair(sections));
	}
}


let cleaningPairs = [];
formPairs();
// console.log(cleaningPairs);
/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

// Get Answer
( function () {
	let answer = 0;
	
	for (let i = 0; i < cleaningPairs.length; i++) {
		if (cleaningPairs[i].overlaps) {
			answer++;
		}
	}
	
	addAnswerToPage(answer);
})();