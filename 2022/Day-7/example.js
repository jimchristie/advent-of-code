/********************************* Get the input ********************************************************/
let separator = '----------------------------------------------------------';
let input = document.getElementById("input").innerHTML.split("\n");
console.log(input);
// console.log('input', input);

let drawing = input.slice(0, input.indexOf('') - 1);
let moves = input.slice(input.indexOf("") + 1, input.length);

console.log('drawing', drawing);


/******************************** Classes ***************************************************************/

class Stack {
	constructor (stackNumber, crates) {
		this.number = stackNumber;
		this.crates = '';
	}
}
/*************************** Procedurals *************************************************************/


function formStacks() {
	let stacks = [];
	
	for (let i = 1; i <= 3; i++) {
		stacks.push('');
	}
	
	for (let i = drawing.length - 1; i >= 0; i--) {
		for (let k = 0; k < drawing[i].length; k++) {
			let foundCrate = drawing[i].slice(k, k + 1) !== ' ' && drawing[i].slice(k, k + 1) !== '[' && drawing[i].slice(k, k + 1) !== ']';
			if (foundCrate) {
				stacks[(k - 1) / 4] += drawing[i].slice(k, k + 1);
			}
		}
	}
	
	return stacks;
}

let stacks = formStacks();

function drawStack(stack) {
	console.log('stack.number', stack.number);
	for (let i = stack.crates.length - 1; i >= 0; i--) {
		console.log(stack.crates[i].label);
	}
	console.log('stack.number', stack.number);
}

function move(instructions) {
	let start = instructions.start - 1;
	let target = instructions.target - 1;
	
	// for (let i = 0; i < instructions.numberOfCrates; i++) {
		// stacks[target] += stacks[start].slice(stacks[start].length - 1, stacks[start].length);
		stacks[target] += stacks[start].slice(stacks[start].length - instructions.numberOfCrates, stacks[start].length);
		stacks[start] = stacks[start].slice(0, stacks[start].length - instructions.numberOfCrates);
	// }
}

function shuffleCrates() {
	console.log('moves', moves);
	
	for (let i = 0; i < moves.length; i++) {
		let formattedMove = moves[i].replace(/[a-z ]/g, ',');
		formattedMove = formattedMove.replace(',,,,,,', ',');
		formattedMove = formattedMove.replace(',,,,,', ',');
		formattedMove = formattedMove.replace(',,,,', ',');
		formattedMove = formattedMove.split(',');
		formattedMove = formattedMove.slice(1, formattedMove.length);
		formattedMove = {
			numberOfCrates: formattedMove[0],
			start: formattedMove[1],
			target: formattedMove[2]
		};
		
		move(formattedMove);
	}
}

shuffleCrates();


/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

// Get Answer
( function () {
	let answer = '';
	
	for (let i = 0; i < stacks.length; i++) {
		answer += stacks[i].slice(stacks[i].length - 1);
	}
	
	addAnswerToPage(answer);
})();