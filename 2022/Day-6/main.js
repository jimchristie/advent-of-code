/********************************* Get the input ********************************************************/
let separator = '----------------------------------------------------------';
let input = document.getElementById("input").innerHTML.split("\n");
input = input[0].split('');
console.log('input', input);


/******************************** Classes ***************************************************************/


/*************************** Procedurals *************************************************************/

function findSignal() {
	for (let i = 13; i < input.length; i++) {
		let signal = true;
		
		for (let j = i - 13; j <= i; j++) {
			for (let k = j; k <= i; k++) {
				if (j !== k && input[j] === input[k]) {
					signal = false;
				}
			}
		}
		
		if (signal) {
			console.log('signal i', i);
			return i + 1;
		}
	}
}



/*********************** Answers ********************************************************************/


function addAnswerToPage(answer) {
	if (typeof answer === "string" || typeof answer === "number"){
		document.getElementById("answer").innerHTML = "Answer: " + answer;
	} else {
		document.getElementById("answer").innerHTML = "Error! <br><br> typeof answer === " + typeof answer;
	}
	
}

// Get Answer
( function () {
	let answer = findSignal();
	
	addAnswerToPage(answer);
})();